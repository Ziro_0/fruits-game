//=======================================================
//global constants
//=======================================================

//Array of fruit names. Also used to create the URLs to load the fruit images
const FRUIT_NAMES = [
  "fruit-apple",
  "fruit-banana",
  "fruit-lemon",
  "fruit-orange",
  "fruit-pear",
  "fruit-strawberry"
];

//Names of all the items.
// Notes: The filename of the image that represents the item must be "item-" followed by of the specified name.
const ITEM_NAME_INVINCIBLE = "invincible";
const ITEM_NAME_LIFE = "life";
const ITEM_NAME_POINTS = "points";
const ITEM_NAME_SLOW = "slow";
const ITEM_NAME_TROLLIN = "trollin";

/*
Array of level data. Each element defines data for a level. The element format is an object:
  minSendFruitDelay. Number. Min delay, in secs, in which new fruit are spawned
  maxSendFruitDelay: Number. Max delay, in secs, in which new fruit are spawned
  minFruitSpeed: Number. Min moving speed of the fruit
  maxFruitSpeed: Number. Max moving speed of the fruit
  maxActiveFruit: integer. Max number of fruit that can appear in the level
  maxActiveItems: integer. Max number of items that can appear in the level
  catchesPerLevel: integer. Number of fruit that must be caught to win the level
*/
const LEVELS_DATA = [
  //Level 0
  {
    minSendFruitDelay: 1.5,
    maxSendFruitDelay: 2.0,
    minFruitSpeed: 3.0,
    maxFruitSpeed: 6.0,
    maxActiveFruit: 1,
    maxActiveItems: 1,
    catchesPerLevel: 8,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 10 }
    ]
  },
  
  //Level 1
  {
    minSendFruitDelay: 1.25,
    maxSendFruitDelay: 1.5,
    minFruitSpeed: 4.0,
    maxFruitSpeed: 8.0,
    maxActiveFruit: 2,
    maxActiveItems: 1,
    catchesPerLevel: 10,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 8 }
    ]
  },
  
  //Level 2
  {
    minSendFruitDelay: 1.0,
    maxSendFruitDelay: 1.2,
    minFruitSpeed: 5.0,
    maxFruitSpeed: 10.0,
    maxActiveFruit: 3,
    maxActiveItems: 1,
    catchesPerLevel: 12,
    items: [
      { name: ITEM_NAME_SLOW, minSpeed: 3.0, maxSpeed: 6.0, spawnChance: 4 }
    ]
  },
  
  //Level 3
  {
    minSendFruitDelay: 0.8,
    maxSendFruitDelay: 1.0,
    minFruitSpeed: 6.0,
    maxFruitSpeed: 12.0,
    maxActiveFruit: 4,
    maxActiveItems: 1,
    catchesPerLevel: 15,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 7 },
      { name: ITEM_NAME_SLOW, minSpeed: 3.0, maxSpeed: 6.0, spawnChance: 3 },
      { name: ITEM_NAME_LIFE, minSpeed: 7.0, maxSpeed: 14.0, spawnChance: 1 }
    ]
  },
  
  //Level 4
  {
    minSendFruitDelay: 0.6,
    maxSendFruitDelay: 1.0,
    minFruitSpeed: 7.0,
    maxFruitSpeed: 14.0,
    maxActiveFruit: 5,
    maxActiveItems: 1,
    catchesPerLevel: 18,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 6 },
      { name: ITEM_NAME_TROLLIN, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 6 }
    ]
  },
  
  //Level 5
  {
    minSendFruitDelay: 0.45,
    maxSendFruitDelay: 0.9,
    minFruitSpeed: 8.0,
    maxFruitSpeed: 16.0,
    maxActiveFruit: 6,
    maxActiveItems: 2,
    catchesPerLevel: 20,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 5 },
      { name: ITEM_NAME_SLOW, minSpeed: 3.0, maxSpeed: 6.0, spawnChance: 3 },
      { name: ITEM_NAME_LIFE, minSpeed: 7.0, maxSpeed: 14.0, spawnChance: 1 },
      { name: ITEM_NAME_INVINCIBLE, minSpeed: 10.0, maxSpeed: 25.0, spawnChance: 1 },
      { name: ITEM_NAME_TROLLIN, minSpeed: 2.5, maxSpeed: 5.0, spawnChance: 6 }
    ]
  },
  
  //Level 6
  {
    minSendFruitDelay: 0.3,
    maxSendFruitDelay: 0.75,
    minFruitSpeed: 9.0,
    maxFruitSpeed: 18.0,
    maxActiveFruit: 7,
    maxActiveItems: 2,
    catchesPerLevel: 30,
    items: [
      { name: ITEM_NAME_POINTS, minSpeed: 5.0, maxSpeed: 10.0, spawnChance: 2 },
      { name: ITEM_NAME_LIFE, minSpeed: 7.0, maxSpeed: 14.0, spawnChance: 12 },
      { name: ITEM_NAME_INVINCIBLE, minSpeed: 10.0, maxSpeed: 25.0, spawnChance: 3 },
      { name: ITEM_NAME_TROLLIN, minSpeed: 1.0, maxSpeed: 3.0, spawnChance: 15 }
    ]
  }
];

//starting player properties
const START_SCORE = 0;
const START_LIVES = 3;
const START_LEVEL = 0;

//Max number of fruit div tags. The pool does not dynamically expand beyond this value, so be sure it's at least as
// large as the maximun number of fruit that can appear on any given level.
const FRUIT_POOL_SIZE = 10;

//Same as FRUIT_POOL_SIZE, but for items.
const ITEM_POOL_SIZE = 1;

//factor that the slow item appects the speed of all fruit while the item is active
const SLOW_SPEED_SCALE = 0.25;

//TROLLIN' parameters >:)
const TROLLIN_SPEED_SCALE = 2.6;
const MAX_ACTIVE_FRUIT_WHILE_TROLLIN = 7;
const POINTS_EARNED_WHILE_TROLLIN = 1;

//the quicker you catch the fruit, the more points you get
//base points per level.
const MIN_POINTS_PER_CATCH = 1;
const MAX_POINTS_PER_CATCH = 13;

//Starting health of each fruit
const FRUIT_HEALTH = 6;

//Frame rate, in frames per second (fps). Determines how fast the enter frame interval should
// run: (1 sec / FRAMES_PER_SECOND) x (1000 ms / 1 sec) will give you the interval in ms per frames.
//The fps should be an integer greater than zero.
const FRAMES_PER_SECOND = 30;

//=======================================================
//global variables
//=======================================================

//flag that determines if the gameplay is active or not
var _isPlaying = false;

//key: integer. The value represents points awarded displays whose number of digits match the key value
//data: array. Contains unused points awarded displays
var _pointsAwardedPool = {};

//key: string. The div id of the fruit being targeted
//data: true
var _fruitsTargeted = {};

//Maintains the next id number when generating points awarded displays
var _nextPointsAwardedSpanId = 0;

//game engine interval callback reference
var _enterFrameInterval = null;

//fruit sending delay
var _sendFruitTicks = -1;

//item sending delay
var _sendItemTicks = 0;

//dimensions of entity container
var _gameContainerWidth = 0;
var _gameContainerHeight = 0;

//player-related data
var _playerData = {
  //player's current score
  score: 0,
  
  //player's current lives remaining
  lives: 0,
  
  //current level the player is on
  level: 0,
  
  //number of fruit the player has caught for the level (resets to 0 for each level)
  numFruitCaught: 0,
  
  //Array of active items. Format of elements:
  // name. String. Name of item representing te item.
  // duration: integer. Duration, in frame ticks, that the item is active.
  // barUiId: String. The progress bar ID presenting this item while it is active.
  items: [ null, null, null ]
};

//number of currently active fruit
var _numActiveFruit = 0;

//number of currently active items
var _numActiveItems = 0;

//pool ids of fruits that are inactive
var _inactiveFruitIds;

//pool ids of items that are inactive
var _inactiveItemIds;

/*
fruit data units representing fruits that are active
 key: String. The fruit id
 data: Object with format:
    id: String. The fruit id
    x: Number. Logical x position of fruit. The float number (keeps track of desimals)
    y: Number. Logical y position of fruit. The float number (keeps track of desimals)
    speed: Number. The speed of the fruit. Moves by this amount, in pixels, per frame
    logistics: Object. Data containing start position and moving direction of fruit
      xStart: integer. Min value of the range of the x value in which the fruit can be placed
      xEnd: integer. Max value of the range of the x value in which the fruit can be placed
      yStart: integer. Min value of the range of the y value in which the fruit can be placed
      yEnd: integer. Max value of the range of the y value in which the fruit can be placed
      xSpeed: integer. Should be -1, 0, or 1. Determines if fruit moves left, right, or not along the x axis
      ySpeed: integer. Should be -1, 0, or 1. Determines if fruit moves up, down, or not along the y axis
    health: integer. The "health" of the fruit. As the mouse is moved over the fruit, the health decreases.
      When the health reaches zero, the fruit is caught.
*/
var _activeFruitData = {};

/*
item data units representing items that are active
 key: String. The fruit id
 data: Object with same format as _activeFruitData (see above)
*/
var _activeItemData = {};

//flag if game is paused or not
var _isPaused = false;


//=======================================================
//jquery intro
//=======================================================

$(function () {
  //=======================================================
  // addToScore
  //=======================================================
  //Adds points to the player's score
  function addToScore(score) {
    setScore(_playerData.score + score);
  }

  //=======================================================
  // getCurrentLevelData
  //=======================================================
  //Returns the current level data block the player is on
  function getCurrentLevelData() {
    return(LEVELS_DATA[_playerData.level]);
  }
  
  //=======================================================
  // getRandomItemData
  //=======================================================
  function getRandomItemData(items) {
    //Returns a random item data block.
    
    var selector = randomRange(0, 100);
    var threshold = 0;
    for (var index = 0; index < items.length; index++) {
      var itemData = items[index];
      threshold = threshold + itemData.spawnChance;
      if (selector < threshold) {
        return(itemData);
      }
      threshold = threshold + itemData.spawnChance;
    }
    return(null);
  }

  //=======================================================
  // calcFruitTicks
  //=======================================================
  function calcFruitTicks() {
    //Calculates and returns the number of ticks needed until the next fruit is generated. Chooses
    // a random value between the min and max ticks for the level for sending fruit.
    //However, if TROLLIN' as active, the ticks are much smaller, resulting in fruit being created
    // at an faster rate
    if (isItemActive(ITEM_NAME_TROLLIN)) {
      return(Math.floor(FRAMES_PER_SECOND / 4));
    }
    
    var levelData = getCurrentLevelData();
    var maxDelayTicks = Math.floor(levelData.maxSendFruitDelay * FRAMES_PER_SECOND);
    var minDelayTicks = Math.floor(levelData.minSendFruitDelay * FRAMES_PER_SECOND);
    return(randomRange(minDelayTicks, maxDelayTicks));
  }
  
  //=======================================================
  // calcPointsFromFruit
  //=======================================================
  function calcPointsFromFruit(fruitId) {
    //Calculates the points awarded from a caught fruit based on how quickly it was caught.
    // However, if TROLLIN is active, points are fixed
    if (isItemActive(ITEM_NAME_TROLLIN)) {
      return(POINTS_EARNED_WHILE_TROLLIN);
    }
    
    var fruitData = _activeFruitData[fruitId]
    var xSpeed = fruitData.logistics.xSpeed;
    var ySpeed = fruitData.logistics.ySpeed;
    
    var fruitRangeStart;
    var fruitRangeEnd;
    var distanceTraveled;
    
    var fruit = $(fruitId);
    var position = fruit.position();
    
    if (xSpeed === 1) {
      fruitRangeStart = -fruit.width();
      fruitRangeEnd = _gameContainerWidth;
      distanceTraveled = Math.abs(position.left - fruitRangeStart);
    } else if (xSpeed === -1) {
      fruitRangeStart = _gameContainerWidth;
      fruitRangeEnd = -fruit.width();
      distanceTraveled = Math.abs(position.left - fruitRangeStart);
    } else if (ySpeed === 1) {
      fruitRangeStart = -fruit.height();
      fruitRangeEnd = _gameContainerHeight;
      distanceTraveled = Math.abs(position.top - fruitRangeStart);
    } else if (ySpeed === -1) {
      fruitRangeStart = _gameContainerHeight;
      fruitRangeEnd = -fruit.height();
      distanceTraveled = Math.abs(position.top - fruitRangeStart);
    }
    
    var fruitRange = Math.abs(fruitRangeEnd - fruitRangeStart);
    var distanceRatio = distanceTraveled / fruitRange;
    var pointsRange = MAX_POINTS_PER_CATCH - MIN_POINTS_PER_CATCH;
    var basePoints = MAX_POINTS_PER_CATCH - Math.floor(pointsRange * distanceRatio);
    var finalPoints = basePoints * (_playerData.level + 1);
    
    if (isItemActive(ITEM_NAME_POINTS)) {
      finalPoints = finalPoints * 2;
    }
    
    return(finalPoints);
  }
  
  //=======================================================
  // canChooseFruit
  //=======================================================
  function canChooseFruit() {
    //Determines if a new fruit can be sent.
    
    if (_inactiveFruitIds.length === 0) {
      //No inactive fruit remaining - fail
      return(false);
    }
    
    var levelData = getCurrentLevelData();
    
    var maxActiveFruit;
    if (isItemActive(ITEM_NAME_TROLLIN)) {
      maxActiveFruit = MAX_ACTIVE_FRUIT_WHILE_TROLLIN;
    } else {
      maxActiveFruit = levelData.maxActiveFruit;
    }
    
    if (_numActiveFruit >= maxActiveFruit) {
      //Max fruit for the level already active -  fail
      return(false);
    }
    
    return(true);
  }
  
  //=======================================================
  // canChooseItem
  //=======================================================
  function canChooseItem() {
    //Determines if a new item can be sent.
    
    if (isItemActive(ITEM_NAME_TROLLIN)) {
      //No items can be created while TROLLIN' is active
      return(false);
    }
    
    if (_inactiveItemIds.length === 0) {
      //No inactive items remaining - fail
      return(false);
    }
    
    var levelData = getCurrentLevelData();
    if (_numActiveItems >= levelData.maxActiveItems) {
      //Max fruit for the level already active -  fail
      return(false);
    }
    
    return(true);
  }
  
  //=======================================================
  // caughtFruit
  //=======================================================
  function caughtFruit(fruitId) {
    if (fruitId in _fruitsTargeted) {
      delete _fruitsTargeted[fruitId];
    }
    
    var points = calcPointsFromFruit(fruitId);
    
    addToScore(points);
    
    if (Math.random() >= 0.5) {
      playSound("snd-get-fruit0");
    } else {
      playSound("snd-get-fruit1");
    }
    
    //var fruitId = "#" + fruit.id;
    var fruitData = _activeFruitData[fruitId]
    
    //stop fruit from moving. If fruit, especially a fast moving fruit, is caught just before
    // it escapes, although the explosion doesn't move, the underlying div still does, and it's
    // possible it could still escape, counting against teh player.
    fruitData.speed = 0;
    
    var xPoints = $(fruitId).position().left + $(fruitId).width() / 2;
    var yPoints = $(fruitId).position().top + $(fruitId).height() / 2;
    $(fruitId).effect("explode", "linear", 500, function() {
      //I don't like function closures. However, I used one here, so I could send
      // arguments/params the function called when the explosion is complete. There doesn't
      // seem to be a way - at least I haven't found any - to specify params in the
      // complete function of an effect.
      onFruitExplodeComplete(fruitId, points, xPoints, yPoints);
    });
    
    if (!isItemActive(ITEM_NAME_TROLLIN)) {
      //It only counts as a caught fruit if TROLLIN' is not active! >:D
      _playerData.numFruitCaught++;
    }
  }
  
  //=======================================================
  // chooseFruit
  //=======================================================
  function chooseFruit() {
    //Gets an inactive fruit, and assigns it a random image
    
    var fruitId = _inactiveFruitIds.pop();
    
    //set random fruit image
    $(fruitId).attr("src", "images/" + randomArrayElement(FRUIT_NAMES) + ".png");
    
    //when the image has completed loading, introduce the fruit into play
    $(fruitId).on("load", onFruitImageLoadComplete);
  }
  
  //=======================================================
  // collectItem
  //=======================================================
  function collectItem(itemName) {
    //this function is called when an item is collected
    
    //check if the player already has this item active
    var itemIndex = findActivePlayerItemIndex(itemName);
    var item;
    
    if (itemIndex === -1) {
      //Player does not already have this item, so set up new data for this item onto the player data
      item = {
        name: itemName,
        maxDuration: FRAMES_PER_SECOND * 10,
        duration: 0,
        barUiId: "item-hud-" + itemName
      };
      
      item.duration = item.maxDuration;
      
      itemIndex = findFreePlayerItemIndex();
      _playerData.items[itemIndex] = item;
      presentItemBar(item);
    } else {
      //Player already has this item - simply reset the duration and that's it
      item = _playerData.items[itemIndex];
      item.duration = item.maxDuration;
    }
  }
  
  //=======================================================
  // createEntityLogisticsData
  //=======================================================
  function createEntityLogisticsData() {
    //Create an entity's (fruit or item) logistics data. Determines which side of the window from which
    // the entity appears, and the direction it moves. Returns the logistics data
    var data;
    
    switch (Math.floor(Math.random() * 4)) {
      case 0: //start at top, move down
        data = { xStart: 0, xEnd: _gameContainerWidth, yStart: 0, yEnd: 0, xSpeed: 0, ySpeed: 1 };
        break;
        
      case 1: //start at bottom, move up
        data = { xStart: 0, xEnd: _gameContainerWidth, yStart: _gameContainerHeight, yEnd: _gameContainerHeight, xSpeed: 0, ySpeed: -1 };
        break;
      
      case 2: //start on left, move right
        data = { xStart: 0, xEnd: 0, yStart: 0, yEnd: _gameContainerHeight, xSpeed: 1, ySpeed: 0 };
        break;
      
      case 3: //start on right, move left
      default:
        data = { xStart: _gameContainerWidth, xEnd: _gameContainerWidth, yStart: 0, yEnd: _gameContainerHeight, xSpeed: -1, ySpeed: 0 };
        break;
    }
    
    return(data);
  }
  
  //=======================================================
  // deactivateFruit
  //=======================================================
  function deactivateFruit(fruitId) {
    //Removes a fruit from play and adds it to the inactive pool
    
    if (_activeFruitData[fruitId] == null) {
      //sanity check - fruit was not active
      return;
    }
    
    $(fruitId).hide();
    $(fruitId).off("load", onFruitImageLoadComplete);
    
    _activeFruitData[fruitId] = null;
    _inactiveFruitIds.push(fruitId);
    _numActiveFruit--;
  }
  
  //=======================================================
  // deactivateItem
  //=======================================================
  function deactivateItem(itemId) {
    //Removes an item from play and adds it to the inactive pool
    
    if (_activeItemData[itemId] == null) {
      //sanity check - item was not active
      return;
    }
    
    $(itemId).hide();
    $(itemId).off("load", onItemImageLoadComplete);
    
    _activeItemData[itemId] = null;
    _inactiveItemIds.push(itemId);
    _numActiveItems--;
  }
  
  //=======================================================
  // enableButton
  //=======================================================
  function enableButton(buttonId, isEnabled) {
    //Displays a button in the enabled or disabled state by adding and removing the approppriate classes
    if (isEnabled === true) {
      $(buttonId).removeClass("disabled-button").addClass("button");
    } else {
      $(buttonId).addClass("disabled-button").removeClass("button");
    }
  }
  
  //=======================================================
  // findActivePlayerItemIndex
  //=======================================================
  function findActivePlayerItemIndex(itemName) {
    //Determines if the player has activated the item by looking for its index within the player data
    // Returns the index within the aray if the item is found, or -1 if it is not.
    
    for (var index = 0; index < _playerData.items.length; index++) {
      var item = _playerData.items[index];
      if (item === null) {
        continue; //sanity check
      } else if (item.name === itemName) {
        //compare items by name
        return(index);
      }
    }
    
    return(-1); //nope
  }
  
  //=======================================================
  // findFreePlayerItemIndex
  //=======================================================
  function findFreePlayerItemIndex(itemName) {
    //Searches for a free (unused) item index within the player data items array. Returns the index of the
    // first free index found, or -1 if they are all occupied.
    for (var index = 0; index < _playerData.items.length; index++) {
      var item = _playerData.items[index];
      if (item === null) {
        return(index);
      }
    }
    
    return(-1); //nope
  }
  
  //=======================================================
  // gameOver
  //=======================================================
  function gameOver() {
    //GAME OVAH!?
    
    //turn off/stop and remove pretty much everything
    _isPlaying = false;

    stopEnterFrame();
    stopSendingFruit();
    
    removeAllFruit();
    removeAllItems();
    removeAllPowerUps();
    
    $("#trials-left").hide();
    $("#game-over").show();
    $("#game-over").html('<p>Thanks for playing!</p><p>Your score is: ' + _playerData.score + '</p>');
    $("#start-reset").html("Start!");
    
    //game no longer in play - disable the pause/resume button
    enableButton("#pause-resume", false);
    
    //Play my game ovah music!
    playSound("snd-game-over");
  }

  //=======================================================
  // handleFruit
  //=======================================================
  function handleFruit() {
    //Proces the fruit each game frame/tick
    
    //Set speed multipler if slow item is active
    var fruitSpeedScale = 1.0;
    if (isItemActive(ITEM_NAME_SLOW)) {
      fruitSpeedScale = SLOW_SPEED_SCALE;
    } else if (isItemActive(ITEM_NAME_TROLLIN)) {
      fruitSpeedScale = TROLLIN_SPEED_SCALE;
    }
    
    for (var fruitId in _activeFruitData) {
      var fruitData = _activeFruitData[fruitId];
      if (fruitData == null) {
        continue; //sanity check
      }
      
      if (fruitId in _fruitsTargeted) {
        fruitData.health--;
        if (fruitData.health <= 0) {
          caughtFruit(fruitId);
          continue;
        }
      }
      
      //advance the position fruit based on its direction and speed
      var xSpeed = fruitData.speed * fruitData.logistics.xSpeed * fruitSpeedScale;
      var ySpeed = fruitData.speed * fruitData.logistics.ySpeed * fruitSpeedScale;
      fruitData.x = fruitData.x + xSpeed;
      fruitData.y = fruitData.y + ySpeed;
      
      $(fruitId).css({ 
        left: Math.floor(fruitData.x),
        top: Math.floor(fruitData.y)
      });
      
      //determine if the fruit "escaped" (if it moves out of the window)
      var didFruitEscape = false;
      if (xSpeed > 0 && fruitData.x > _gameContainerWidth) {
        didFruitEscape = true;
      } else if (xSpeed < 0 && fruitData.x < -$(fruitId).width()) {
        didFruitEscape = true;
      } else if (ySpeed > 0 && fruitData.y > _gameContainerHeight) {
        didFruitEscape = true;
      } else if (ySpeed < 0 && fruitData.y < -$(fruitId).height()) {
        didFruitEscape = true;
      }
      
      if (didFruitEscape) {
        if (isItemActive(ITEM_NAME_INVINCIBLE) === false) {
          //If the ivincible item is active the player will not lose a life when the fruit escapes
          missedFruit(fruitId);
        }
        
        deactivateFruit(fruitId);
      }
    }
  }
  
  //=======================================================
  // handleFruitCreation
  //=======================================================
  function handleFruitCreation() {
    //Determines if new fruit can be created
    
    if (_sendFruitTicks <= -1) {
      //not sending any new fruits
      return;
    }
    
    //Decrease the send fruit ticks by 1 each frame. When it reaches 0, try to create a new fruit, then
    // reset the send fruit ticks.
    _sendFruitTicks--;
    
    if (_sendFruitTicks <= 0) {
      if (canChooseFruit() === true) {
        chooseFruit();
      }
      
      startSendingFruit();
    }
  }
  
  //=======================================================
  // handleItemCreation
  //=======================================================
  function handleItemCreation() {
    //Determines if a new item can be created
    
    _sendItemTicks++;
    if (_sendItemTicks < FRAMES_PER_SECOND) {
      return;
    }
    
    _sendItemTicks = 0;
    
    if (canChooseItem() === false) {
      return;
    }
    
    var levelData = getCurrentLevelData();
    var itemData = getRandomItemData(levelData.items);
    if (itemData === null) {
      return;
    }

    //set random item image, set item data and continue to initialize the item once its image has loaded
    var itemId = _inactiveItemIds.pop();
    $(itemId).attr("src", "images/item-" + itemData.name + ".png");
    $(itemId).data("itemData", itemData);
    $(itemId).on("load", onItemImageLoadComplete);
  }
  
  //=======================================================
  // handleItemEffect
  //=======================================================
  function handleItemEffect(divItemId) {
    //Process an item that was just caught by the player. Determine wha ttype of item it is, and handle
    // it according to the item's name.
    
    var itemData = $(divItemId).data("itemData");
    
    switch (itemData.name) {
      case ITEM_NAME_INVINCIBLE:
        handleItemEffectInvincible(divItemId, itemData, "Invincible");
        break;
        
      case ITEM_NAME_LIFE:
        handleItemEffectLife(divItemId, itemData, "Extra Life");
        break;
        
      case ITEM_NAME_POINTS:
        handleItemEffectPoints(divItemId, itemData, "Points x2");
        break;
        
      case ITEM_NAME_SLOW:
        handleItemEffectSlow(divItemId, itemData, "Sloooow");
        break;
        
      case ITEM_NAME_TROLLIN:
        handleItemEffectTrollin(divItemId, itemData, "TROLLIN!");
        break;
        
      default:
        console.log("jquery:handleItemEffect. Warning: Unknown item id: '" + itemId + "'.");
        break;
    }
  }
  
  //=======================================================
  // explodeItemAndPresentMessage
  //=======================================================
  function explodeItemAndPresentMessage(divItemId, message) {
    var position = $(divItemId).position();
    var xPoints = position.left + $(divItemId).width() / 2;
    var yPoints = position.top + $(divItemId).height() / 2;
    $(divItemId).effect("explode", "linear", 500, function() {
      onItemExplodeComplete(divItemId, xPoints, yPoints, message);
    });
  }
  
  //=======================================================
  // handleItemEffectInvincible
  //=======================================================
  function handleItemEffectInvincible(divItemId, itemData, message) {
    playSound("snd-item-invincible");
    explodeItemAndPresentMessage(divItemId, message);
    collectItem(itemData.name);
  }
  
  //=======================================================
  // handleItemEffectLife
  //=======================================================
  function handleItemEffectLife(divItemId, itemData, message) {
    if (_playerData.lives < 3) {
      playSound("snd-item-extra-life");
      setLives(_playerData.lives + 1);
      collectItem(itemData.name);
      explodeItemAndPresentMessage(divItemId, message);
      return;
    }
    
    handleItemEffectPoints(divItemId, itemData, "Points x2", ITEM_NAME_POINTS);
  }
  
  //=======================================================
  // handleItemEffectPoints
  //=======================================================
  function handleItemEffectPoints(divItemId, itemData, message, itemName) {
    playSound("snd-item-points");
    explodeItemAndPresentMessage(divItemId, message);
    if (itemName === undefined) {
      itemName = itemData.name;
    }
    collectItem(itemName);
  }
  
  //=======================================================
  // handleItemEffectSlow
  //=======================================================
  function handleItemEffectSlow(divItemId, itemData, message) {
    playSound("snd-item-slow");
    explodeItemAndPresentMessage(divItemId, message);
    collectItem(itemData.name);
  }
  
  //=======================================================
  // handleItemEffectTrollin
  //=======================================================
  function handleItemEffectTrollin(divItemId, itemData, message) {
    playSound("snd-item-trollin");
    explodeItemAndPresentMessage(divItemId, message);
    removeAllItems();
    removeAllPowerUps();
    collectItem(itemData.name);
  }
  
  //=======================================================
  // handleItems
  //=======================================================
  function handleItems() {
    for (var itemId in _activeItemData) {
      var itemData = _activeItemData[itemId];
      if (itemData == null) {
        continue;
      }
      
      var xSpeed = itemData.speed * itemData.logistics.xSpeed;
      var ySpeed = itemData.speed * itemData.logistics.ySpeed;
      itemData.x = itemData.x + xSpeed;
      itemData.y = itemData.y + ySpeed;
      
      $(itemId).css({ 
        left: Math.floor(itemData.x),
        top: Math.floor(itemData.y)
      });
      
      var didEntityEscape = false;
      if (xSpeed > 0 && itemData.x > _gameContainerWidth) {
        didEntityEscape = true;
      } else if (xSpeed < 0 && itemData.x < -$(itemId).width()) {
        didEntityEscape = true;
      } else if (ySpeed > 0 && itemData.y > _gameContainerHeight) {
        didEntityEscape = true;
      } else if (ySpeed < 0 && itemData.y < -$(itemId).height()) {
        didEntityEscape = true;
      }
      
      if (didEntityEscape) {
        deactivateItem(itemId);
      }
    }
  }
  
  //=======================================================
  // handlePowerUps
  //=======================================================
  function handlePowerUps() {
    for (var index = _playerData.items.length - 1; index >= 0; index--) {
      var item = _playerData.items[index];
      if (item === null) {
        continue;
      }
      
      if (--item.duration === 0) {
        unpresentItemBar(item);
        _playerData.items[index] = null;
      } else {
        presentItemBar(item);
      }
    }
  }
  
  //=======================================================
  // initFruitEventHandlers
  //=======================================================
  function initFruitEventHandlers() {
    for (var index = 0; index < _inactiveFruitIds.length; index++) {
      var fruitId = _inactiveFruitIds[index];
      $(fruitId).mouseleave(onFruitMouseLeave);
      $(fruitId).mouseenter(onFruitMouseEnter);
    }
  }
  
  //=======================================================
  // initFruitPools
  //=======================================================
  function initFruitPools() {
    _inactiveFruitIds = [];
    
    for (var index = 0; index < FRUIT_POOL_SIZE; index++) {
      var fruitId = "fruit" + index;
      $("#fruit-container").append('<img id="' + fruitId + '" class="fruit">');
      
      _inactiveFruitIds.push("#" + fruitId);
    }
  }
  
  //=======================================================
  // initItemEventHandlers
  //=======================================================
  function initItemEventHandlers() {
    for (var index = 0; index < _inactiveItemIds.length; index++) {
      var id = _inactiveItemIds[index];
      $(id).mousedown(onItemMouseDown);
      $(id).mouseenter(onItemMouseEnter);
    }
  }
  
  //=======================================================
  // initItemBarUi
  //=======================================================
  function initItemBarUi(nameSpec, barWidth, barHeight) {
    $(nameSpec).progressbar();
    $(nameSpec).width(barWidth);
    $(nameSpec).height(barHeight);
  }
  
  //=======================================================
  // initItemBarUis
  //=======================================================
  function initItemBarUis() {
    //hide all item bar uis at once
    $(".item-hud").css({visibility: "hidden"});
    
    //create all the item progress bars at once
    initItemBarUi(".item-hud-bar", 100, 10);
    
    //set TROLLIN' bar specifically
    initItemBarUi("#item-hud-bar-trollin", 340, 10)
  }
  
  //=======================================================
  // initItemPools
  //=======================================================
  function initItemPools() {
    _inactiveItemIds = [];
    
    for (var index = 0; index < ITEM_POOL_SIZE; index++) {
      var itemId = "item" + index;
      $("#fruit-container").append('<img id="' + itemId + '" class="item">');
      
      _inactiveItemIds.push("#" + itemId);
    }
  }
  
  //=======================================================
  // isItemUpActive
  //=======================================================
  function isItemActive(itemName) {
    var length = _playerData.items.length;
    for (var index = 0; index < length; index++) {
      var item = _playerData.items[index];
      if (item !== null && item.name == itemName) {
        return(true);
      }
    }
    
    return(false);
  }
  
  //=======================================================
  // keepInRange
  //=======================================================
  function keepInRange(value, min, max) {
    value = Math.min(value, max);
    value = Math.max(value, min);
    return(value);
  }
  
  //=======================================================
  // missedFruit
  //=======================================================
  function missedFruit() {
    if (_playerData.lives > 1) {
      playSound("snd-missed-fruit");
      setLives(_playerData.lives - 1);
    } else {
      gameOver();
    }
  }

  //=======================================================
  // onCreditsLinkClick
  //=======================================================
  function onCreditsLinkClick() {
    if (_isPlaying === true && _isPaused === false) {
      setPaused(true);
    }
  }
  
  //=======================================================
  // onEnterFrame
  //=======================================================
  function onEnterFrame() {
    if (_isPaused === true) {
      return;
    }
    
    handleFruit();
    handleFruitCreation();
    
    handleItems();
    handleItemCreation();
    
    handlePowerUps();
  }

  //=======================================================
  // onFruitExplodeComplete
  //=======================================================
  function onFruitExplodeComplete(fruitId, points, xPoints, yPoints) {
    presentMessage(points, xPoints, yPoints);
    
    deactivateFruit(fruitId);
    
    var levelData = getCurrentLevelData();
    if (_playerData.numFruitCaught >= levelData.catchesPerLevel) {
      wonLevel();
    }
  }
  
  //=======================================================
  // onFruitImageLoadComplete
  //=======================================================
  function onFruitImageLoadComplete(event) {
    var fruitImage = event.currentTarget;
    var fruitId = "#" + fruitImage.id;
    
    var logisticsData = createEntityLogisticsData();
    var levelData = getCurrentLevelData();
    var fruitData = {
      id: fruitId,
      x: randomRange(logisticsData.xStart, logisticsData.xEnd),
      y: randomRange(logisticsData.yStart, logisticsData.yEnd),
      speed: randomRangeFloat(levelData.minFruitSpeed, levelData.maxFruitSpeed),
      logistics: logisticsData,
      health: FRUIT_HEALTH
    };
    
    fruitData.x = randomRange(logisticsData.xStart, logisticsData.xEnd);
    fruitData.y = randomRange(logisticsData.yStart, logisticsData.yEnd);
    
    if (logisticsData.xSpeed !== 0) {
      if (fruitData.x === 0) {
        fruitData.x -= fruitImage.width;
      } else {
        fruitData.x = _gameContainerWidth;
      }
      
      if (fruitData.y + fruitImage.height >= _gameContainerHeight) {
        fruitData.y = _gameContainerHeight - fruitImage.height;
      } else if (fruitData.y < 0) {
        fruitData.y = 0;
      }
    }
    
    if (logisticsData.ySpeed !== 0) {
      if (fruitData.y === 0) {
        fruitData.y -= fruitImage.height;
      } else {
        fruitData.y = _gameContainerHeight;
      }
      
      if (fruitData.x + fruitImage.width >= _gameContainerWidth) {
        fruitData.x = _gameContainerWidth - fruitImage.width;
      } else if (fruitData.x < 0) {
        fruitData.x = 0;
      }
    }
    
    _activeFruitData[fruitId] = fruitData;

    //set random position
    $(fruitId).css({
      left: Math.floor(fruitData.x),
      top: Math.floor(fruitData.y)
    });

    $(fruitId).show();
    _numActiveFruit++;
  }
  
  //=======================================================
  // onFruitMouseEnter
  //=======================================================
  function onFruitMouseEnter(event) {
    var fruitImage = event.currentTarget;
    var fruitId = "#" + fruitImage.id;
    
    //if TROLLIN' is active, fruits can be collected by touch - a little sanity to this madness, sheesh
    if (isItemActive(ITEM_NAME_TROLLIN)) {
      caughtFruit(fruitId);
    } else {
      _fruitsTargeted[fruitId] = true;
    }
  }
  
  //=======================================================
  // onFruitMouseLeave
  //=======================================================
  function onFruitMouseLeave(event) {
    var fruitImage = event.currentTarget;
    var fruitId = "#" + fruitImage.id;
    
    if (_fruitsTargeted[fruitId] === undefined) {
      return; //sanity checks
    }
    
    delete _fruitsTargeted[fruitId];
  }
  
  //=======================================================
  // onItemExplodeComplete
  //=======================================================
  function onItemExplodeComplete(divItemId, xPoints, yPoints, message) {
    if (message != undefined) {
      presentMessage(message, xPoints, yPoints);
    }
    
    deactivateItem(divItemId);
  }
  
  //=======================================================
  // onItemImageLoadComplete
  //=======================================================
  function onItemImageLoadComplete(event) {
    var itemImage = event.currentTarget;
    var itemId = "#" + itemImage.id;
    
    var logisticsData = createEntityLogisticsData();
    var levelData = getCurrentLevelData();
    var dbItemData = $(itemId).data("itemData");
    
    var itemData = {
      id: itemId,
      x: randomRange(logisticsData.xStart, logisticsData.xEnd),
      y: randomRange(logisticsData.yStart, logisticsData.yEnd),
      speed: randomRangeFloat(dbItemData.minSpeed, dbItemData.maxSpeed),
      logistics: logisticsData
    };
    
    itemData.x = randomRange(logisticsData.xStart, logisticsData.xEnd);
    itemData.y = randomRange(logisticsData.yStart, logisticsData.yEnd);
    
    if (logisticsData.xSpeed !== 0) {
      if (itemData.x === 0) {
        itemData.x -= itemImage.width;
      } else {
        itemData.x = _gameContainerWidth;
      }
      
      if (itemData.y + itemImage.height >= _gameContainerHeight) {
        itemData.y = _gameContainerHeight - itemImage.height;
      } else if (itemData.y < 0) {
        itemData.y = 0;
      }
    }
    
    if (logisticsData.ySpeed !== 0) {
      if (itemData.y === 0) {
        itemData.y -= itemImage.height;
      } else {
        itemData.y = _gameContainerHeight;
      }
      
      if (itemData.x + itemImage.width >= _gameContainerWidth) {
        itemData.x = _gameContainerWidth - itemImage.width;
      } else if (itemData.x < 0) {
        itemData.x = 0;
      }
    }
    
    _activeItemData[itemId] = itemData;

    $(itemId).css({
      left: Math.floor(itemData.x),
      top: Math.floor(itemData.y)
    });

    $(itemId).show();
    _numActiveItems++;
  }
  
  //=======================================================
  // onItemMouseDown
  //=======================================================
  function onItemMouseDown(event) {
    var item = event.currentTarget;
    
    var divItemId = "#" + item.id;
    var itemData = _activeItemData[divItemId];
    
    //stop item from moving. If fruit, especially a fast moving fruit, is caught just before
    // it escapes, although the explosion doesn't move, the underlying div still does, and it's
    // possible it could still escape, counting against the player.
    itemData.speed = 0;
    
    handleItemEffect(divItemId);
  }
  
  //=======================================================
  // onItemMouseEnter
  //=======================================================
  function onItemMouseEnter(event) {
    var item = event.currentTarget;
    var divItemId = "#" + item.id;
    var divItemData = $(divItemId).data("itemData");
    
    //only the TROLLIN' item can be collected without having to click on it - it is picked up upon touch
    // mwa ha! >:)
    if (divItemData.name != ITEM_NAME_TROLLIN) {
      return;
    }
    
    var itemData = _activeItemData[divItemId];
    itemData.speed = 0;
    handleItemEffect(divItemId);
  }
  
  //=======================================================
  // onPauseResumeClick
  //=======================================================
  function onPauseResumeClick() {
    if ($("#pause-resume").hasClass("disabled-button")) {
      return;
    }
    
    playSound("snd-button-click");
    setPaused(!_isPaused);
  }

  //=======================================================
  // onStartResetClick
  //=======================================================
  function onStartResetClick() {
    playSound("snd-button-click");
    
    //check if game is playing
    if (_isPlaying == true) {
      //game is playing, reload page
      location.reload();
    } else {
      //game is not playing, initialize game
      _isPlaying = true;
      
      //reset player data
      setScore(START_SCORE);
      setLives(START_LIVES);
      setLevel(START_LEVEL);
      _playerData.numFruitCaught = 0;
      
      //show trials left box
      $("#trials-left").show();
      
      //hide game over message if playing from a previous game
      $("#game-over").hide();
      
      //change button text to reset game
      $("#start-reset").html("Reset");
      
      startEnterFrame();
      startSendingFruit();
      
      enableButton("#pause-resume", true);
    }
  }    
  
  //=======================================================
  // playSound
  //=======================================================
  function playSound(soundId) {
    //play sound using jquery
    //the audio element returns an array, so we want the first element
    $("#" + soundId)[0].play();
    
    //play sound using DOM
    //document.getElementById(soundId).play();
  }
  
  //=======================================================
  // presentItemBar
  //=======================================================
  function presentItemBar(item) {
    var itemBarUiId = "#" + item.barUiId;
    $(itemBarUiId).css("visibility", "visible");
    
    var value = Math.floor(item.duration / item.maxDuration * 100);
    var progressBarUiId = "#item-hud-bar-" + item.name;
    $(progressBarUiId).progressbar("option", "value", value);
  }
  
  //=======================================================
  // presentMessage
  //=======================================================
  function presentMessage(value, x, y) {
    //Presents a message by animating the characters individually, in a fancy "damage numbers"-type style, what
    // one might see in an RPG game.
    
    var valueString = value.toString();
    var numValueDigits = valueString.length;
    
    var pointsAwardedPool = _pointsAwardedPool[numValueDigits];
    if (pointsAwardedPool == null) {
      pointsAwardedPool = _pointsAwardedPool[numValueDigits] = [];
    }
    
    var pointsAwardedSpanId = pointsAwardedPool.pop();
    var htmlId;
    var sIndex;
    var valueChar;
    var selHtmlId;
    
    if (pointsAwardedSpanId == null) {
      _nextPointsAwardedSpanId++;
      htmlId = "points-awarded" + _nextPointsAwardedSpanId;
    
      var html = '<span id="' + htmlId + '" class="' + 'points-awarded' + '">';
      var sIndex;

      for (sIndex = 0; sIndex < numValueDigits; sIndex++) {
        valueChar = valueString.charAt(sIndex);
        var spanClass = "points-awarded-char";
        html = html + '<span id="' + htmlId + sIndex.toString() + '"' +
          'class="' + spanClass + '">' + valueChar + '</span>';
      }

      html = html + '</span>';
      
      $("#game-ui").append(html);
      
      selHtmlId = "#" + htmlId;
    } else {
      htmlId = pointsAwardedSpanId;
      selHtmlId = "#" + htmlId;
      for (sIndex = 0; sIndex < numValueDigits; sIndex++) {
        valueChar = valueString.charAt(sIndex);
        $("#" + htmlId + sIndex.toString()).html(valueChar);
      }
    }
    
    //The original x is the center point of the entity for which the points display is representing.
    // Center the points display about the entity.
    x = x - $(selHtmlId).width() / 2;
    
    //Ensure the points display, including the bounce, is fully visible
    var xStart = keepInRange(x, 0, _gameContainerWidth - $(selHtmlId).width());
    var yStart = keepInRange(y, 30, _gameContainerHeight - 30);
    
    $(selHtmlId).css({
      left: xStart,
      top: yStart,
      
      //restore visibility of the points display, in case this isn't the first time it's been used
      display: "inline"
    });
    
    var charDelay = 0;
    
    //min num chars =  1  => rate = 100
    //max num chars >= 8  => rate = 25
    //The rate at which the invidiual characters are presented should be faster (smaller rate value) for
    // messages with more characters in then, than message with fewer characters. For example, using the message
    // "super-long message", the characters would be presented faster than in the message, "12".
    var charDelayRate = Math.floor(-10.714 * numValueDigits + 110.714);
    charDelayRate = keepInRange(charDelayRate, 25, 100);
    
    for (sIndex = 0; sIndex < numValueDigits; sIndex++) {
      var sCharId = "#" + htmlId + sIndex.toString();
      $(sCharId).css({
        top: $(sCharId).position().top - 30,
        opacity: 0
      });
      
      $(sCharId)
        .delay(charDelay)
        .animate({
          opacity: 1
        }, 1)
        .animate({
          top: "+=30px"
        }, 300, "easeOutBounce");
      
      charDelay = charDelay + charDelayRate;
    }
    
    $(selHtmlId)
      .delay(numValueDigits * 100 + 300)
      .fadeOut(250, function() {
        pointsAwardedPool.push(htmlId);
      });
  }
  
  //=======================================================
  // randomArrayElement
  //=======================================================
  function randomArrayElement(array) {
    var index = Math.floor(Math.random() * array.length);
    return(array[index]);
  }
  
  //=======================================================
  // randomRange
  //=======================================================
  function randomRange(min, max) {
    return(Math.floor(min + Math.random() * (max - min)));
  }
  
  //=======================================================
  // randomRangeFloat
  //=======================================================
  function randomRangeFloat(min, max) {
    return(min + Math.random() * (max - min));
  }
  
  //=======================================================
  // removeAllFruit
  //=======================================================
  function removeAllFruit() {
    for (var fruitId in _activeFruitData) {
      deactivateFruit(fruitId);
    }
  }
  
  //=======================================================
  // removeAllItems
  //=======================================================
  function removeAllItems() {
    for (var itemId in _activeItemData) {
      deactivateItem(itemId);
    }
  }
  
  //=======================================================
  // removeAllPowerUps
  //=======================================================
  function removeAllPowerUps() {
    for (var index = 0; index < _playerData.items.length; index++) {
      var item = _playerData.items[index];
      if (item === null) {
        continue;
      }
      
      unpresentItemBar(item);
      _playerData.items[index] = null;
    }
  }
  
  //=======================================================
  // setLevel
  //=======================================================
  function setLevel(level) {
    _playerData.level = level;
    $("#levelValue").html(_playerData.level + 1);
  }
  
  //=======================================================
  // setLives
  //=======================================================
  function setLives(lives) {
    _playerData.lives = lives;
    updateLivesUi();
  }
  
  //=======================================================
  // setPaused
  //=======================================================
  function setPaused(isPaused) {
    _isPaused = isPaused;
    
    if (_isPaused) {
      playSound("snd-paused");
      
      $("#paused").show();
      
      //update button text
      $("#pause-resume").html("Resume");
    } else {
      $("#paused").hide();
      
      //update button text
      $("#pause-resume").html("Pause");
    }
  }
  
  //=======================================================
  // setScore
  //=======================================================
  function setScore(score) {
    _playerData.score = score;
    
    //pad the score value in the ui with zeros to make a four-digit score
    var numZeros;
    if (_playerData.score == 0) {
      numZeros = 3;
    } else {
      numZeros = 3 - Math.floor(Math.log10(_playerData.score));
    }
    
    var scoreString = "";
    for (var index = 0; index < numZeros; index++) {
      scoreString = scoreString + "0";
    }
    
    scoreString = scoreString + _playerData.score.toString();
    $("#scoreValue").html(scoreString);
  }

  //=======================================================
  // startEnterFrame
  //=======================================================
  function startEnterFrame() {
    if (_enterFrameInterval == null) { //sanity check
      var msPerFrame = 1000 / FRAMES_PER_SECOND;
      _enterFrameInterval = setInterval(onEnterFrame, msPerFrame);
    }
  }

  //=======================================================
  // startSendingFruit
  //=======================================================
  function startSendingFruit() {
    _sendFruitTicks = calcFruitTicks();
  }
  
  //=======================================================
  // stopEnterFrame
  //=======================================================
  function stopEnterFrame() {
    if (_enterFrameInterval != null) {
      clearInterval(_enterFrameInterval);
      _enterFrameInterval = null;
    }
  }
  
  //=======================================================
  // stopSendingFruit
  //=======================================================
  function stopSendingFruit() {
    _sendFruitTicks = -1;
  }
  
  //=======================================================
  // unpresentItemBar
  //=======================================================
  function unpresentItemBar(item) {
    var itemBarUiId = "#" + item.barUiId;
    $(itemBarUiId).css("visibility", "hidden");
  }
  
  //=======================================================
  // updateLivesUi
  //=======================================================
  function updateLivesUi() {
    $("#trials-left").empty();
    for (var i = 0; i < _playerData.lives; i++) {
      $("#trials-left").append('<img class="life" src="images/heart-icon.png" />');
    }
  }
  
  //=======================================================
  // wonGame
  //=======================================================
  function wonGame() {
    _isPlaying = false;
    
    stopEnterFrame();
    stopSendingFruit();
    removeAllFruit();
    removeAllItems();
    removeAllPowerUps();
    playSound("snd-game-complete");
    enableButton("#pause-resume", false);
    
    $("#game-over").show();
    $("#game-over").html('<p>Congratulations!</p><p>You won!</p>');
  }
  
  //=======================================================
  // wonLevel
  //=======================================================
  function wonLevel() {
    var level = _playerData.level + 1;
    if (level >= LEVELS_DATA.length) {
      wonGame();
      return;
    }
    
    playSound("snd-level-complete");
    _playerData.numFruitCaught = 0;
    setLevel(level);
    startSendingFruit();
  }
  
  //=======================================================
  // - entry code -
  //=======================================================
  _gameContainerWidth = $("#fruit-container").width();
  _gameContainerHeight = $("#fruit-container").height();

  initFruitPools();
  initItemPools();
  initItemBarUis();
  
  $("#start-reset").click(onStartResetClick);
  $("#pause-resume").click(onPauseResumeClick);
  $("#credits-link").click(onCreditsLinkClick);
  
  initFruitEventHandlers();
  initItemEventHandlers();
});